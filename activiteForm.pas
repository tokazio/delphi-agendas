unit activiteForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, DateUtils, types, zDataset, TmetroFormU;

type
  TForm2 = class(TmetroForm)
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Memo1: TMemo;
    Label3: TLabel;
    DateTimePicker1: TDateTimePicker;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    ComboBox3: TComboBox;
    Button1: TButton;
    Button2: TButton;
    DateTimePicker2: TDateTimePicker;
    DateTimePicker3: TDateTimePicker;
    ListBox1: TListBox;
    Edit2: TEdit;
    Label7: TLabel;
    Button3: TButton;
    ListBox2: TListBox;
    Button4: TButton;
    ListBox3: TListBox;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure ListBox2Click(Sender: TObject);
    procedure DateTimePicker3Change(Sender: TObject);
    procedure DateTimePicker2Change(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure ComboBox3DrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure ComboBox3Change(Sender: TObject);
    procedure ListBox3Click(Sender: TObject);
    procedure Edit1Exit(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure Edit1DblClick(Sender: TObject);
  private
    { Private declarations }
    procedure majCategories;
    procedure majFin;
    procedure calcDuree;
    procedure valid;
    procedure propose(const t: string; const showListe: boolean = true);
  public
    { Public declarations }
    procedure clear;
    function load(q: Tzquery): Integer;
    function nouveau: Integer;
  end;

var
  Form2: TForm2;

implementation

uses mainform;
{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
begin
  if DateTimePicker3.DateTime < DateTimePicker2.DateTime then
  begin
    showmessage('L''heure de fin ne peut �tre avant celle de d�but!');
    exit;
  end;

  Modalresult := mrOk;
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
  close;
end;

procedure TForm2.Button3Click(Sender: TObject);
begin
  if ListBox1.visible = false then
  begin
    ListBox1.visible := true;
    Button3.Caption := '\/';
  end
  else
  begin
    ListBox1.visible := false;
    Button3.Caption := '/\';
  end;
end;

procedure TForm2.Button4Click(Sender: TObject);
begin
  if ListBox2.visible = false then
  begin
    ListBox2.visible := true;
    Button4.Caption := '\/';
  end
  else
  begin
    ListBox2.visible := false;
    Button4.Caption := '/\';
  end;
end;

function TForm2.nouveau: Integer;
begin
  clear;
  Result := showmodal;
end;

function TForm2.load(q: Tzquery): Integer;
var
  I: Integer;
begin
  Edit1.Text := q.FieldByName('titre').AsString;
  DateTimePicker1.Date := q.FieldByName('date').AsDateTime;
  Memo1.Text := q.FieldByName('description').AsString;
  DateTimePicker2.Time := q.FieldByName('debut').AsDateTime;
  DateTimePicker3.Time := q.FieldByName('fin').AsDateTime;
  calcDuree;
  majFin;
  for I := 0 to ComboBox3.items.Count - 1 do
    if ComboBox3.items.valueFromIndex[I] = q.FieldByName('categorie').AsString then
    begin
      ComboBox3.ItemIndex := I;
      break;
    end;
  valid;
  Result := showmodal;
end;

procedure TForm2.ListBox1Click(Sender: TObject);
begin
  if ListBox1.ItemIndex > -1 then
    DateTimePicker2.Time := strToTime(ListBox1.items[ListBox1.ItemIndex]);
  // DateTimePicker3.Time := inchour(DateTimePicker2.Time, 1);
  DateTimePicker3.Time := DateTimePicker2.Time + strToTime(Edit2.Text);
  calcDuree;
  majFin;
  Button3Click(self);
end;

procedure TForm2.ListBox2Click(Sender: TObject);
begin
  if ListBox2.ItemIndex > -1 then
    DateTimePicker3.Time := strToTime(copy(ListBox2.items[ListBox2.ItemIndex], 1, 5));
  calcDuree;
  Button4Click(self);
end;

procedure TForm2.ListBox3Click(Sender: TObject);
begin
  if ListBox3.ItemIndex > -1 then
  begin
    Edit1.Text := ListBox3.items[ListBox3.ItemIndex];
    ListBox3.visible := false;
  end;
end;

procedure TForm2.clear;
begin
  Edit1.Text := '';
  Memo1.clear;
  // fin (� calculer)
  DateTimePicker3.DateTime := incMinute(now, 60);
  ListBox2.items.LoadFromFile(extractfilepath(application.ExeName) + 'heures.txt');
  majFin;
  // cat�gorie
  majCategories;
  //
  ComboBox3.Text := '';
end;

procedure TForm2.ComboBox3Change(Sender: TObject);
begin
  valid;
end;

procedure TForm2.ComboBox3DrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  with Control as TComboBox do
  begin
    Canvas.brush.Color := stringToColor(TComboBox(Control).items.Names[Index]);
    Canvas.TextRect(Rect, Rect.Left + 13 + 10, Rect.Top, TComboBox(Control).items.valueFromIndex[Index]);
  end;
end;

procedure TForm2.DateTimePicker2Change(Sender: TObject);
begin
  // DateTimePicker3.Time := inchour(DateTimePicker2.Time, 1);
  DateTimePicker3.Time := DateTimePicker2.Time + strToTime(Edit2.Text);
  calcDuree;
  majFin;
end;

procedure TForm2.DateTimePicker3Change(Sender: TObject);
begin
  calcDuree;
end;

procedure TForm2.Edit1Change(Sender: TObject);
begin
  valid;
end;

procedure TForm2.Edit1DblClick(Sender: TObject);
begin
  if not ListBox3.visible then
    propose('*', false);
  ListBox3.visible := not ListBox3.visible;
end;

procedure TForm2.propose(const t: string; const showListe: boolean = true);
begin
  if (t = '*') or (t = '') then
    form1.ZQuery1.SQL.Text := 'SELECT DISTINCT titre FROM activites ORDER BY titre'
  else
    form1.ZQuery1.SQL.Text := 'SELECT DISTINCT titre FROM activites WHERE titre LIKE ''' + t + '%'' ORDER BY titre';
  form1.ZQuery1.Open;
  if form1.ZQuery1.RecordCount > 0 then
  begin
    form1.ZQuery1.First;
    ListBox3.items.BeginUpdate;
    ListBox3.items.clear;
    while not form1.ZQuery1.Eof do
    begin
      ListBox3.items.Add(form1.ZQuery1.FieldByName('titre').AsString);
      form1.ZQuery1.Next;
    end;
    ListBox3.items.EndUpdate;
    if showListe then
      ListBox3.visible := true;
  end
  else if showListe then
    ListBox3.visible := false;
end;

procedure TForm2.Edit1Exit(Sender: TObject);
begin
  if not ListBox3.Focused then
    ListBox3.visible := false;
end;

procedure TForm2.Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  propose(Edit1.Text);
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  // d�but
  DateTimePicker2.DateTime := now;
  ListBox1.items.LoadFromFile(extractfilepath(application.ExeName) + 'heures.txt');
  DateTimePicker1.DateTime := now;
end;

procedure TForm2.majFin;
var
  m, t: Ttime;
  I: Integer;
begin
  ListBox2.items.BeginUpdate;
  ListBox2.items.clear;
  t := incMinute(DateTimePicker2.Time, 15);
  m := incMinute(strToTime('17:15:00'), 15);
  while (compareTime(t, m) = LessThanValue) do
  begin
    ListBox2.items.Add(formatDateTime('HH:nn', t) + '(' + formatDateTime('HH:nn', t - DateTimePicker2.Time) + ')');
    t := incMinute(t, 15);
  end;
  ListBox2.items.EndUpdate;
end;

procedure TForm2.majCategories;
begin
  ComboBox3.items.NameValueSeparator := '=';
  ComboBox3.items.BeginUpdate;
  ComboBox3.items.clear;
  form1.ZQuery1.SQL.Text := 'SELECT * FROM categories';
  form1.ZQuery1.Open;
  form1.ZQuery1.First;
  while not form1.ZQuery1.Eof do
  begin
    ComboBox3.items.Add(form1.ZQuery1.FieldByName('couleur').AsString + '=' + form1.ZQuery1.FieldByName('titre').AsString);
    form1.ZQuery1.Next;
  end;
  form1.ZQuery1.close;
  ComboBox3.items.EndUpdate;
end;

procedure TForm2.calcDuree;
begin
  Edit2.Text := formatDateTime('HH:nn', DateTimePicker3.Time - DateTimePicker2.Time);
end;

procedure TForm2.valid;
begin
  Button1.Enabled := Edit1.Text <> '';
  Button1.Enabled := ComboBox3.ItemIndex > -1;
end;

end.
