unit statAllForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Spin, TmetroFormU, TmetroSeparatorU, ExtCtrls;

type
  TForm5 = class(TmetroForm)
    ListView1: TListView;
    Label1: TLabel;
    SpinEdit1: TSpinEdit;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    CheckBox1: TCheckBox;
    Panel1: TPanel;
    metroSeparator1: TmetroSeparator;
    procedure SpinEdit1Change(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form5: TForm5;

implementation

uses mainform;

{$R *.dfm}

procedure TForm5.CheckBox1Click(Sender: TObject);
begin
  form1.statall;
end;

procedure TForm5.FormCreate(Sender: TObject);
begin
  appColor:=$0A15FF;
end;

procedure TForm5.RadioButton1Click(Sender: TObject);
begin
  form1.statall;
end;

procedure TForm5.SpinEdit1Change(Sender: TObject);
begin
  form1.statall;
end;

end.
