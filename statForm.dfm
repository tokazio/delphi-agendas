object Form4: TForm4
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = 'Statistiques'
  ClientHeight = 393
  ClientWidth = 320
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Segoe UI Light'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    320
    393)
  PixelsPerInch = 96
  TextHeight = 21
  object Label1: TLabel
    Left = 192
    Top = 32
    Width = 119
    Height = 21
    Anchors = [akTop, akRight]
    Caption = 'Temps total pass'#233
  end
  object Label2: TLabel
    Left = 8
    Top = 32
    Width = 78
    Height = 21
    Caption = 'Occurences'
  end
  object PaintBox1: TPaintBox
    AlignWithMargins = True
    Left = 1
    Top = 92
    Width = 318
    Height = 281
    Margins.Left = 1
    Margins.Top = 0
    Margins.Right = 1
    Margins.Bottom = 0
    Align = alBottom
    Anchors = [akLeft, akTop, akRight, akBottom]
    OnPaint = PaintBox1Paint
  end
  object Edit1: TEdit
    Left = 192
    Top = 56
    Width = 121
    Height = 29
    Anchors = [akTop, akRight]
    Enabled = False
    TabOrder = 0
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 8
    Top = 56
    Width = 77
    Height = 29
    Enabled = False
    TabOrder = 1
    Text = 'Edit1'
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 1
    Top = 373
    Width = 318
    Height = 19
    Margins.Left = 1
    Margins.Top = 0
    Margins.Right = 1
    Margins.Bottom = 1
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 2
    object RadioButton1: TRadioButton
      Left = 4
      Top = 0
      Width = 93
      Height = 17
      Caption = 'Au mois'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI Light'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TabStop = True
      OnClick = RadioButton1Click
    end
    object RadioButton2: TRadioButton
      Left = 100
      Top = 0
      Width = 113
      Height = 17
      Caption = 'Au total'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI Light'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = RadioButton1Click
    end
  end
end
