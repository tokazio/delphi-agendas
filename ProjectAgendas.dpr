program ProjectAgendas;

uses
  Forms,
  mainform in 'mainform.pas' {Form1},
  activiteForm in 'activiteForm.pas' {Form2},
  totalForm in 'totalForm.pas' {Form3},
  statForm in 'statForm.pas' {Form4},
  camembertU in 'camembertU.pas',
  statAllForm in 'statAllForm.pas' {Form5};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Agendas';
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm3, Form3);
  Application.CreateForm(TForm5, Form5);
  Application.Run;
end.
