object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'R'#233'sum'#233
  ClientHeight = 577
  ClientWidth = 918
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Segoe UI Light'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 21
  object PaintBox1: TPaintBox
    AlignWithMargins = True
    Left = 300
    Top = 42
    Width = 617
    Height = 534
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 1
    Margins.Bottom = 1
    Align = alClient
    OnPaint = PaintBox1Paint
    ExplicitLeft = 272
    ExplicitTop = 136
    ExplicitWidth = 105
    ExplicitHeight = 105
  end
  object Splitter1: TSplitter
    AlignWithMargins = True
    Left = 297
    Top = 42
    Height = 534
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 1
    ExplicitLeft = 208
    ExplicitTop = 50
    ExplicitHeight = 531
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 1
    Top = 1
    Width = 916
    Height = 41
    Margins.Left = 1
    Margins.Top = 1
    Margins.Right = 1
    Margins.Bottom = 0
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 0
    DesignSize = (
      916
      41)
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 73
      Height = 21
      Caption = 'P'#233'riode du'
    end
    object Label2: TLabel
      Left = 216
      Top = 8
      Width = 17
      Height = 21
      Caption = 'au'
    end
    object Label3: TLabel
      Left = 376
      Top = 8
      Width = 132
      Height = 21
      Caption = 'Grouper '#224' partir de '
    end
    object Label4: TLabel
      Left = 560
      Top = 8
      Width = 59
      Height = 21
      Caption = 'heure(s).'
    end
    object metroSeparator1: TmetroSeparator
      Left = 0
      Top = 37
      Width = 916
      Height = 4
      color = 4473924
      align = alBottom
      drawHeight = 1
      drawSide = msBottom
      ExplicitTop = 1
    end
    object DateTimePicker1: TDateTimePicker
      Left = 88
      Top = 4
      Width = 121
      Height = 29
      Date = 41645.565132685190000000
      Time = 41645.565132685190000000
      TabOrder = 0
      OnChange = DateTimePicker1Change
    end
    object DateTimePicker2: TDateTimePicker
      Left = 240
      Top = 4
      Width = 121
      Height = 29
      Date = 41645.565132685190000000
      Time = 41645.565132685190000000
      TabOrder = 1
      OnChange = DateTimePicker2Change
    end
    object CheckBox1: TCheckBox
      Left = 798
      Top = 4
      Width = 129
      Height = 29
      Anchors = [akTop, akRight]
      Caption = 'Par cat'#233'gorie'
      Checked = True
      State = cbChecked
      TabOrder = 2
      OnClick = CheckBox1Click
    end
    object DateTimePicker3: TDateTimePicker
      Left = 512
      Top = 4
      Width = 45
      Height = 29
      Date = 41645.125000000000000000
      Format = 'H'
      Time = 41645.125000000000000000
      Kind = dtkTime
      TabOrder = 3
      OnChange = DateTimePicker3Change
    end
  end
  object Panel2: TPanel
    AlignWithMargins = True
    Left = 1
    Top = 42
    Width = 296
    Height = 534
    Margins.Left = 1
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 1
    Align = alLeft
    BevelOuter = bvNone
    Caption = 'Panel2'
    ShowCaption = False
    TabOrder = 1
    object Splitter2: TSplitter
      Left = 0
      Top = 313
      Width = 296
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      Color = clWhite
      ParentColor = False
      ExplicitTop = 0
      ExplicitWidth = 312
    end
    object metroSeparator2: TmetroSeparator
      Left = 292
      Top = 0
      Width = 4
      Height = 313
      color = clRed
      align = alRight
      drawHeight = 1
      drawSide = msRight
      ExplicitTop = -91
      ExplicitHeight = 309
    end
    object ListView1: TListView
      Left = 0
      Top = 0
      Width = 292
      Height = 313
      Margins.Left = 1
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 1
      Align = alClient
      BorderStyle = bsNone
      Columns = <
        item
          AutoSize = True
          Caption = 'Cat'#233'gorie'
        end
        item
          Caption = 'Temps total'
          Width = 100
        end>
      ReadOnly = True
      RowSelect = True
      ShowColumnHeaders = False
      TabOrder = 0
      ViewStyle = vsReport
    end
    object Panel3: TPanel
      Left = 0
      Top = 316
      Width = 296
      Height = 218
      Align = alBottom
      BevelOuter = bvNone
      Caption = 'Panel3'
      ShowCaption = False
      TabOrder = 1
      DesignSize = (
        296
        218)
      object metroSeparator3: TmetroSeparator
        Left = 0
        Top = 0
        Width = 296
        Height = 5
        color = clRed
        align = alTop
        drawHeight = 1
        drawSide = msTop
      end
      object Label5: TLabel
        Left = 4
        Top = 4
        Width = 135
        Height = 21
        Caption = 'Cat'#233'gories '#224' afficher'
      end
      object metroSeparator4: TmetroSeparator
        Left = 292
        Top = 5
        Width = 4
        Height = 213
        color = clRed
        align = alRight
        drawHeight = 1
        drawSide = msRight
        ExplicitLeft = 286
        ExplicitTop = 40
        ExplicitHeight = 309
      end
      object ListBox1: TListBox
        Left = 0
        Top = 28
        Width = 293
        Height = 190
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsNone
        ItemHeight = 21
        MultiSelect = True
        TabOrder = 0
        OnClick = ListBox1Click
      end
    end
  end
end
