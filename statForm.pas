unit statForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, camembertU, TmetroFormU;

type
  TForm4 = class(TmetroForm)
    Label1: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Label2: TLabel;
    PaintBox1: TPaintBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    Panel1: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure PaintBox1Paint(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
  private
    { Private declarations }
    Fbuffer:Tbitmap;
    procedure drawGraph;
  public
    { Public declarations }
    tot:integer;
    totmois:integer;
    totall:integer;
    titre:string;
    couleur:Tcolor;
  end;

var
  Form4: TForm4;

implementation

{$R *.dfm}

procedure TForm4.FormCreate(Sender: TObject);
begin
  Fbuffer:=Tbitmap.Create;
end;

procedure TForm4.FormDestroy(Sender: TObject);
begin
  Fbuffer.free;
end;

procedure TForm4.PaintBox1Paint(Sender: TObject);
begin
  drawGraph;
  paintbox1.Canvas.Draw(0,0,Fbuffer);
end;

procedure TForm4.RadioButton1Click(Sender: TObject);
begin
  paintbox1.Repaint;
end;

procedure TForm4.drawGraph;
var
  I: Integer;
  l,c:Tstringlist;
begin
  l:=Tstringlist.Create;
  c:=Tstringlist.Create;
  Fbuffer.SetSize(PaintBox1.Width, PaintBox1.height);
  //valeurs
  l.add(titre+'='+inttostr(tot));
  if radiobutton1.checked then
  begin
    l.add('autres='+inttostr(totmois-tot));
    l.add('total='+inttostr(totmois));
  end;
  if radiobutton2.checked then
  begin
    l.add('autres='+inttostr(totall-tot));
    l.add('total='+inttostr(totall));
  end;
  //couleurs de catégorie
  c.add(titre+'='+colorToString(couleur));
  c.add('autres='+colorToString(clblue));

  drawCamembert(Fbuffer.Canvas,l,c);
  l.Free;
  c.Free;
end;

end.
