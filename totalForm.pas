unit totalForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls, dateutils, Math, camembertU, TmetroFormU,
  TmetroSeparatorU;

type
  TForm3 = class(TmetroForm)
    PaintBox1: TPaintBox;
    ListView1: TListView;
    Panel1: TPanel;
    Label1: TLabel;
    DateTimePicker1: TDateTimePicker;
    Label2: TLabel;
    DateTimePicker2: TDateTimePicker;
    CheckBox1: TCheckBox;
    Label3: TLabel;
    DateTimePicker3: TDateTimePicker;
    Label4: TLabel;
    metroSeparator1: TmetroSeparator;
    Splitter1: TSplitter;
    metroSeparator2: TmetroSeparator;
    Panel2: TPanel;
    ListBox1: TListBox;
    Panel3: TPanel;
    Splitter2: TSplitter;
    metroSeparator3: TmetroSeparator;
    Label5: TLabel;
    metroSeparator4: TmetroSeparator;
    procedure DateTimePicker1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DateTimePicker2Change(Sender: TObject);
    procedure PaintBox1Paint(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure DateTimePicker3Change(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
  private
    { Private declarations }
    Fbuffer: Tbitmap;
    Fcouleurs: Tstringlist;
    procedure affiche;
    procedure drawGraph;
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

uses mainform;
{$R *.dfm}

procedure TForm3.CheckBox1Click(Sender: TObject);
begin
  affiche;
end;

procedure TForm3.DateTimePicker1Change(Sender: TObject);
var
  myYear, myMonth, myDay: word;
begin
  decodeDate(DateTimePicker1.Date, myYear, myMonth, myDay);
  DateTimePicker2.Date := encodedate(myYear, myMonth, DaysInMonth(DateTimePicker1.Date));
  affiche;
end;

procedure TForm3.DateTimePicker2Change(Sender: TObject);
begin
  affiche;
end;

procedure TForm3.DateTimePicker3Change(Sender: TObject);
begin
  affiche;
end;

procedure TForm3.FormCreate(Sender: TObject);
var
  myYear, myMonth, myDay: word;
  i: integer;
begin
  Fbuffer := Tbitmap.Create;
  Fcouleurs := Tstringlist.Create;
  decodeDate(now, myYear, myMonth, myDay);
  DateTimePicker1.Date := encodedate(myYear, myMonth, 1);
  DateTimePicker2.Date := encodedate(myYear, myMonth, DaysInMonth(DateTimePicker1.Date));

  ListBox1.Items.BeginUpdate;
  ListBox1.Items.Clear;
  form1.zquery1.SQL.Text := 'SELECT * FROM categories';
  form1.zquery1.open;
  form1.zquery1.First;
  while not form1.zquery1.Eof do
  begin
    i := ListBox1.Items.add(form1.zquery1.FieldByName('titre').AsString);
    ListBox1.Selected[i] := true;
    form1.zquery1.Next;
  end;
  form1.zquery1.Close;
  ListBox1.Items.EndUpdate;
end;

procedure TForm3.FormDestroy(Sender: TObject);
begin
  Fbuffer.Free;
  Fcouleurs.Free;
end;

procedure TForm3.FormShow(Sender: TObject);
begin
  affiche;
end;

procedure TForm3.ListBox1Click(Sender: TObject);
begin
  affiche;
end;

procedure TForm3.PaintBox1Paint(Sender: TObject);
begin
  drawGraph;
  PaintBox1.Canvas.Draw(0, 0, Fbuffer);
end;

procedure TForm3.affiche;
var
  it: Tlistitem;
  i: integer;
  t: Tstringlist;
  titreIn: string;
begin
  t := Tstringlist.Create;
  t.delimiter := ',';
  for i := 0 to ListBox1.Items.Count - 1 do
    if ListBox1.Selected[i] then
      t.add('''' + ListBox1.Items[i] + '''');
  titreIn := t.delimitedText;
  t.Free;

  // Couleurs
  Fcouleurs.Clear;
  form1.zquery1.SQL.Text := 'SELECT * FROM categories WHERE titre IN (' + titreIn + ')';
  form1.zquery1.open;
  form1.zquery1.First;
  while not form1.zquery1.Eof do
  begin
    Fcouleurs.add(form1.zquery1.FieldByName('titre').AsString + '=' + form1.zquery1.FieldByName('couleur').AsString);
    form1.zquery1.Next;
  end;
  form1.zquery1.Close;

  if CheckBox1.Checked then
  begin
    ListView1.Items.BeginUpdate;
    ListView1.Clear;
    form1.zquery1.SQL.Text := 'SELECT sum(duree) as c,categorie FROM (SELECT fin-debut as duree,categorie FROM activites WHERE categorie IN (' + titreIn + ') AND date BETWEEN :da AND :db) AS a GROUP BY categorie ORDER BY c DESC';
    form1.zquery1.ParamByName('da').Value := dateToStr(DateTimePicker1.Date);
    form1.zquery1.ParamByName('db').Value := dateToStr(DateTimePicker2.Date);
    form1.zquery1.open;
    form1.zquery1.First;
    while not form1.zquery1.Eof do
    begin
      it := ListView1.Items.add;
      it.caption := form1.zquery1.FieldByName('categorie').AsString;
      it.subitems.add(form1.zquery1.FieldByName('c').AsString);
      form1.zquery1.Next;
    end;
    form1.zquery1.Close;

    form1.zquery1.SQL.Text := 'SELECT sum(duree) as c FROM (SELECT fin-debut as duree,categorie FROM activites WHERE categorie IN (' + titreIn + ') AND date BETWEEN :da AND :db) as a';
    form1.zquery1.open;
    form1.zquery1.First;

    it := ListView1.Items.add;
    it.caption := 'Total';
    it.subitems.add(form1.zquery1.FieldByName('c').AsString);

    form1.zquery1.Close;
    ListView1.Items.EndUpdate;
  end
  else
  begin
    ListView1.Items.BeginUpdate;
    ListView1.Clear;
    form1.zquery1.SQL.Text := 'SELECT sum(duree) as c,titre FROM (SELECT fin-debut as duree,titre FROM activites WHERE categorie IN (' + titreIn + ') AND date BETWEEN :da AND :db) AS a GROUP BY titre ORDER BY c DESC';
    form1.zquery1.ParamByName('da').Value := dateToStr(DateTimePicker1.Date);
    form1.zquery1.ParamByName('db').Value := dateToStr(DateTimePicker2.Date);
    form1.zquery1.open;
    form1.zquery1.First;
    while not form1.zquery1.Eof do
    begin
      it := ListView1.Items.add;
      it.caption := form1.zquery1.FieldByName('titre').AsString;
      it.subitems.add(form1.zquery1.FieldByName('c').AsString);
      form1.zquery1.Next;
    end;
    form1.zquery1.Close;

    form1.zquery1.SQL.Text := 'SELECT sum(duree) as c FROM (SELECT fin-debut as duree,titre FROM activites WHERE categorie IN (' + titreIn + ') AND date BETWEEN :da AND :db) as a';
    form1.zquery1.open;
    form1.zquery1.First;

    it := ListView1.Items.add;
    it.caption := 'Total';
    it.subitems.add(form1.zquery1.FieldByName('c').AsString);

    form1.zquery1.Close;
    ListView1.Items.EndUpdate;
  end;
  PaintBox1.Repaint;
end;

function htexttosec(htext: string): int64;
var
  h, m, s: string;
  hi, mi, si: integer;
begin
  h := copy(htext, 1, pos(':', htext));
  m := copy(htext, pos(':', htext) + 1, 2);
  s := copy(htext, pos(':', htext) + 4, 2);
  tryStrToInt(h, hi);
  tryStrToInt(m, mi);
  tryStrToInt(s, si);
  Result := hi * 60 * 60 + mi * 60 + si;
end;

procedure TForm3.drawGraph;
var
  i: integer;
  l: Tstringlist;
  a: integer;
  h, m, s, ms: word;
begin
  l := Tstringlist.Create;
  Fbuffer.SetSize(PaintBox1.Width, PaintBox1.height);
  if CheckBox1.Checked then
  begin
    for i := 0 to ListView1.Items.Count - 1 do
      l.add(ListView1.Items[i].caption + '=' + inttostr(htexttosec(ListView1.Items[i].subitems[0])));
  end
  else
  begin
    decodeTime(DateTimePicker3.Time, h, m, s, ms);
    a := 0;
    for i := 0 to ListView1.Items.Count - 2 do
      if htexttosec(ListView1.Items[i].subitems[0]) >= (60 * 60 * h) then
        l.add(ListView1.Items[i].caption + '=' + inttostr(htexttosec(ListView1.Items[i].subitems[0])))
      else
      begin
        a := a + htexttosec(ListView1.Items[i].subitems[0]);
      end;
    l.add('Autres=' + inttostr(a));
    l.add(ListView1.Items[ListView1.Items.Count - 1].caption + '=' + inttostr(htexttosec(ListView1.Items[ListView1.Items.Count - 1].subitems[0])));
  end;
  drawCamembert(Fbuffer.Canvas, l, Fcouleurs);
  l.Free;
end;

end.
