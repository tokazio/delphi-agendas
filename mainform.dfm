object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Agendas'
  ClientHeight = 709
  ClientWidth = 1094
  Color = clWhite
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Segoe UI Light'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 21
  object PaintBox1: TPaintBox
    AlignWithMargins = True
    Left = 1
    Top = 69
    Width = 1092
    Height = 598
    Margins.Left = 1
    Margins.Top = 0
    Margins.Right = 1
    Margins.Bottom = 0
    Align = alClient
    ParentShowHint = False
    PopupMenu = PopupMenu1
    ShowHint = True
    OnClick = PaintBox1Click
    OnDblClick = PaintBox1DblClick
    OnMouseDown = PaintBox1MouseDown
    OnMouseMove = PaintBox1MouseMove
    OnMouseUp = PaintBox1MouseUp
    OnPaint = PaintBox1Paint
    ExplicitLeft = 452
    ExplicitTop = 320
    ExplicitWidth = 105
    ExplicitHeight = 105
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 1
    Top = 1
    Width = 1092
    Height = 68
    Margins.Left = 1
    Margins.Top = 1
    Margins.Right = 1
    Margins.Bottom = 0
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 0
    DesignSize = (
      1092
      68)
    object Label2: TLabel
      AlignWithMargins = True
      Left = 15
      Top = 3
      Width = 223
      Height = 62
      Margins.Left = 15
      Margins.Right = 10
      Align = alLeft
      Caption = 'Semaine n'#176'?'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -43
      Font.Name = 'Segoe UI Light'
      Font.Style = []
      ParentFont = False
      ExplicitHeight = 59
    end
    object DateTimePicker1: TDateTimePicker
      Left = 932
      Top = 20
      Width = 122
      Height = 29
      Anchors = [akTop, akRight]
      Date = 41645.448602118050000000
      Time = 41645.448602118050000000
      TabOrder = 0
      OnChange = DateTimePicker1Change
    end
    object Button2: TButton
      Left = 1060
      Top = 20
      Width = 29
      Height = 29
      Hint = 'Semaine suivante'
      ParentCustomHint = False
      Anchors = [akTop, akRight]
      Caption = '>'
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 896
      Top = 20
      Width = 29
      Height = 29
      Hint = 'Semaine pr'#233'c'#233'dente'
      Anchors = [akTop, akRight]
      Caption = '<'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 777
      Top = 20
      Width = 101
      Height = 29
      Anchors = [akTop, akRight]
      Caption = 'R'#233'sum'#233
      TabOrder = 3
      OnClick = Button4Click
    end
  end
  object Panel2: TPanel
    AlignWithMargins = True
    Left = 1
    Top = 667
    Width = 1092
    Height = 41
    Margins.Left = 1
    Margins.Top = 0
    Margins.Right = 1
    Margins.Bottom = 1
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 1
    DesignSize = (
      1092
      41)
    object Label1: TLabel
      Left = 129
      Top = 16
      Width = 958
      Height = 17
      Anchors = [akRight, akBottom]
      Caption = 
        'Utiliser les fl'#232'ches sur une s'#233'l'#232'ction pour la d'#233'placer. Double ' +
        'clic pour modifier. Cliquer sur x pour supprimer. Attendre 1 sec' +
        'onde sur une case pour afficher le commentaire.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI Light'
      Font.Style = []
      ParentFont = False
      ExplicitLeft = 131
    end
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 101
      Height = 29
      Caption = '+ Ajouter'
      TabOrder = 0
      OnClick = Button1Click
    end
  end
  object ZConnection1: TZConnection
    ControlsCodePage = cCP_UTF16
    Port = 0
    Protocol = 'postgresql-9'
    Left = 488
    Top = 360
  end
  object ZQuery1: TZQuery
    Connection = ZConnection1
    Params = <>
    Left = 644
    Top = 368
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 508
    Top = 480
  end
  object PopupMenu1: TPopupMenu
    OnPopup = PopupMenu1Popup
    Left = 836
    Top = 496
    object Statistiques1: TMenuItem
      Caption = 'Statistiques'
      OnClick = Statistiques1Click
    end
  end
end
