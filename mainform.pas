unit mainform;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, ZAbstractRODataset, ZAbstractDataset,
  ZDataset, ZAbstractConnection, ZConnection, ComCtrls, dateutils, Menus, TmetroformU, Math;

type

  Tboxliste = class;

  Tbox = class
  Private
    Fr: Trect;
    Fid: integer;
    Ftitre: string;
    Fdescription: string;
    Fcouleur: Tcolor;
    Fparent: Tboxliste;
    Fstate: integer;
  Published
    property r: Trect read Fr;
    property id: integer read Fid;
    property titre: string read Ftitre;
    property description: string read Fdescription;
    property couleur: Tcolor read Fcouleur;
  Public
    constructor create(parent: Tboxliste; r: Trect; q: Tzquery);
    procedure draw;
    procedure select;
    procedure move(ctrl: boolean);
    procedure deselect;
    procedure hover;
    function click(x, y: integer): Tbox;
    procedure delete;
  end;

  Tboxliste = class
  Private
    Fcanvas: Tcanvas;
    FbufferMove: Tbitmap;
    Fitems: Tlist;
    FonDelete: TnotifyEvent;
    procedure setItem(index: integer; val: Tbox);
    function getItem(index: integer): Tbox;
    procedure delete(box: Tbox);
  Published
    property canvas: Tcanvas read Fcanvas;
    property onDelete: TnotifyEvent read FonDelete write FonDelete;
  Public
    property items[index: integer]: Tbox read getItem write setItem; default;
    function intersect(x, y: integer; click: boolean = false): Tbox;
    procedure add(r: Trect; q: Tzquery);
    procedure clear;
    function select(id: integer): Tbox;
    constructor create(canvas: Tcanvas; bufferMove: Tbitmap);
    destructor free;
  end;

  TForm1 = class(TmetroForm)
    PaintBox1: TPaintBox;
    Panel1: TPanel;
    ZConnection1: TZConnection;
    ZQuery1: Tzquery;
    DateTimePicker1: TDateTimePicker;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Timer1: TTimer;
    Panel2: TPanel;
    Label1: TLabel;
    Button1: TButton;
    Label2: TLabel;
    PopupMenu1: TPopupMenu;
    Statistiques1: TMenuItem;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PaintBox1Paint(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure PaintBox1MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; x, y: integer);
    procedure PaintBox1DblClick(Sender: TObject);
    procedure PaintBox1MouseMove(Sender: TObject; Shift: TShiftState; x, y: integer);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormResize(Sender: TObject);
    procedure PaintBox1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure Statistiques1Click(Sender: TObject);
    procedure PaintBox1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; x, y: integer);
  private
    { Private declarations }
    Fbuffer: Tbitmap;
    FbufferMove: Tbitmap;
    FbufferGrid: Tbitmap;
    Fboxs: Tboxliste;
    FselectedBox: Tbox;
    FhoveredBox: Tbox;
    FlastSelectedId: integer;
    xclick, yclick: integer;
    xmouse, ymouse: integer;
    clickon, ismoving: boolean;
    ctrl, dblclick: boolean;
    procedure drawAgendas;
    function drawCaseHeures(y, h: integer; b: string): integer;
    procedure deleteBox(Sender: TObject);
    procedure drawGrid;
    procedure ajouter;
  public
    { Public declarations }
    procedure statall;
  end;

const
  LARGEURCOLHEURES: integer = 50;
  MOVESTART: integer = 5;

var
  Form1: TForm1;

implementation

uses activiteForm, totalForm, statForm, statAllForm;
{$R *.dfm}

function lundi(dateTime: TdateTime): TdateTime overload;
var
  myYear, myMonth, myDay, w: Word;
begin
  // jours
  decodeDate(dateTime, myYear, myMonth, myDay);
  w := WeekOfTheYear(dateTime);
  if (myMonth = 12) and (w = 1) then
    myYear := myYear + 1;
  // lundi de la semaine de la date choisie
  Result := StartOfAWeek(myYear, w, 1);
end;

function lundi(date: Tdate): Tdate overload;
var
  myYear, myMonth, myDay, w: Word;
begin
  // jours
  decodeDate(date, myYear, myMonth, myDay);
  w := WeekOfTheYear(date);
  if (myMonth = 12) and (w = 1) then
    myYear := myYear + 1;
  // lundi de la semaine de la date choisie
  Result := StartOfAWeek(myYear, w, 1);
end;

function htexttosec(htext: string): int64;
var
  h, m, s: string;
  hi, mi, si: integer;
begin
  h := copy(htext, 1, pos(':', htext));
  m := copy(htext, pos(':', htext) + 1, 2);
  s := copy(htext, pos(':', htext) + 4, 2);
  tryStrToInt(h, hi);
  tryStrToInt(m, mi);
  tryStrToInt(s, si);
  Result := hi * 60 * 60 + mi * 60 + si;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  form2.DateTimePicker2.dateTime := now;
  form2.DateTimePicker1.dateTime := now;
  ajouter;
end;

procedure TForm1.ajouter;
begin
  if form2.nouveau = mrOk then
  begin
    ZQuery1.SQL.Text := 'INSERT INTO activites(titre,date,debut,fin,categorie,description) VALUES(:titre,:date,:debut,:fin,:categorie,:description)';
    ZQuery1.ParamByName('titre').Value := form2.Edit1.Text;
    ZQuery1.ParamByName('date').Value := formatDateTime('yyyy-mm-dd', form2.DateTimePicker1.date);
    ZQuery1.ParamByName('debut').Value := formatDateTime('HH:nn:00', form2.DateTimePicker2.Time);
    ZQuery1.ParamByName('fin').Value := formatDateTime('HH:nn:00', form2.DateTimePicker3.Time);
    ZQuery1.ParamByName('categorie').Value := form2.ComboBox3.items.ValueFromIndex[form2.ComboBox3.ItemIndex];
    ZQuery1.ParamByName('description').Value := form2.Memo1.Lines.Text;
    try
      ZQuery1.ExecSQL;
    except
      on E: Exception do
        showmessage('Erreur: ' + E.Message);
    end;
    drawAgendas;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  f: string;
  c: Tstringlist;
begin
  Fbuffer := Tbitmap.create;
  FbufferMove := Tbitmap.create;
  FbufferGrid := Tbitmap.create;
  Fboxs := Tboxliste.create(Fbuffer.canvas, FbufferMove);

  Fboxs.onDelete := deleteBox;

  c := Tstringlist.create;
  f := extractfilepath(application.ExeName) + 'config.txt';
  if fileexists(f) then
  begin
    c.loadFromfile(f);
    ZConnection1.HostName := c.Values['host'];
    ZConnection1.Port := strtoint(c.Values['port']);
    ZConnection1.database := c.Values['database'];
    ZConnection1.User := c.Values['user'];
    ZConnection1.Password := c.Values['password'];
  end;
  ZConnection1.Connect;
  c.free;

  // nettoyage base
  ZQuery1.SQL.Text := 'DELETE FROM activites WHERE fin<debut';
  ZQuery1.ExecSQL;

  DateTimePicker1.date := lundi(now);

  drawGrid;
  drawAgendas;
  Label2.Caption := 'Semaine n�' + inttostr(WeekOfTheYear(DateTimePicker1.dateTime));
end;

procedure TForm1.deleteBox(Sender: TObject);
begin
  ZQuery1.SQL.Text := 'DELETE FROM activites WHERE id=' + inttostr(Tbox(Sender).id);
  ZQuery1.ExecSQL;
  drawAgendas;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  Fbuffer.free;
  FbufferMove.free;
  FbufferGrid.free;
  Fboxs.free;
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  t, tf: Ttime;
  d: Tdate;
  c: integer;
begin
  if FselectedBox <> nil then
  begin

    if (Key = vk_up) then
    begin
      ZQuery1.SQL.Text := 'SELECT debut,fin FROM activites WHERE id=''' + inttostr(FselectedBox.id) + '''';
      ZQuery1.Open;
      ZQuery1.First;
      t := ZQuery1.FieldByName('debut').AsDateTime;
      tf := ZQuery1.FieldByName('fin').AsDateTime;
      ZQuery1.Close;
      ZQuery1.SQL.Text := 'UPDATE activites SET debut=''' + timeToStr(incminute(t, -15)) + ''',fin=''' + timeToStr(incminute(tf, -15)) + ''' WHERE id=' + inttostr(FselectedBox.id);
      ZQuery1.ExecSQL;
      drawAgendas;
      Key := 0;
    end

    else if (Key = vk_down) then
    begin
      ZQuery1.SQL.Text := 'SELECT debut,fin FROM activites WHERE id=''' + inttostr(FselectedBox.id) + '''';
      ZQuery1.Open;
      ZQuery1.First;
      t := ZQuery1.FieldByName('debut').AsDateTime;
      tf := ZQuery1.FieldByName('fin').AsDateTime;
      ZQuery1.Close;
      ZQuery1.SQL.Text := 'UPDATE activites SET debut=''' + timeToStr(incminute(t, 15)) + ''',fin=''' + timeToStr(incminute(tf, 15)) + ''' WHERE id=' + inttostr(FselectedBox.id);
      ZQuery1.ExecSQL;
      drawAgendas;
      Key := 0;
    end

    else if (Key = vk_left) then
    begin
      ZQuery1.SQL.Text := 'SELECT date FROM activites WHERE id=''' + inttostr(FselectedBox.id) + '''';
      ZQuery1.Open;
      ZQuery1.First;
      d := ZQuery1.FieldByName('date').AsDateTime;
      ZQuery1.Close;
      // si c'est lundi alors je passe au vendredi avant soit -3 sinon jour d'avant -1
      if DayOfWeek(d) = 2 then
        c := -3
      else
        c := -1;
      ZQuery1.SQL.Text := 'UPDATE activites SET date=''' + dateToStr(incday(d, c)) + ''' WHERE id=' + inttostr(FselectedBox.id);
      ZQuery1.ExecSQL;
      drawAgendas;
      Key := 0;
    end

    else if (Key = vk_right) then
    begin
      ZQuery1.SQL.Text := 'SELECT date FROM activites WHERE id=''' + inttostr(FselectedBox.id) + '''';
      ZQuery1.Open;
      ZQuery1.First;
      d := ZQuery1.FieldByName('date').AsDateTime;
      ZQuery1.Close;
      // si c'est vendredi alors je passe au lundi apres soit 3 sinon jour d'apres 1
      if DayOfWeek(d) = 6 then
        c := 3
      else
        c := 1;
      ZQuery1.SQL.Text := 'UPDATE activites SET date=''' + dateToStr(incday(d, c)) + ''' WHERE id=' + inttostr(FselectedBox.id);
      ZQuery1.ExecSQL;
      drawAgendas;
      Key := 0;
    end;
  end;
end;

procedure TForm1.FormResize(Sender: TObject);
begin
  drawGrid;
  drawAgendas;
end;

procedure TForm1.PaintBox1Click(Sender: TObject);
begin
  Form1.SetFocus;
end;

procedure TForm1.PaintBox1DblClick(Sender: TObject);
var
  w, h: integer;
begin
  dblclick := true;
  if FselectedBox <> nil then
  begin
    form2.clear;
    ZQuery1.SQL.Text := 'SELECT * FROM activites WHERE id=' + inttostr(FselectedBox.id);
    ZQuery1.Open;
    if ZQuery1.RecordCount = 0 then
    begin
      showmessage('Pas d''enregistrement avec l''id ' + inttostr(FselectedBox.id));
      ZQuery1.Close;
      exit;
    end;
    ZQuery1.First;
    if form2.load(ZQuery1) = mrOk then
    begin
      ZQuery1.SQL.Text := 'UPDATE activites SET titre=:titre,date=:date,debut=:debut,fin=:fin,categorie=:categorie,description=:description WHERE id=' + inttostr(FselectedBox.id);
      ZQuery1.ParamByName('titre').Value := form2.Edit1.Text;
      ZQuery1.ParamByName('date').Value := formatDateTime('yyyy-mm-dd', form2.DateTimePicker1.date);
      ZQuery1.ParamByName('debut').Value := formatDateTime('HH:nn:00', form2.DateTimePicker2.Time);
      ZQuery1.ParamByName('fin').Value := formatDateTime('HH:nn:00', form2.DateTimePicker3.Time);
      ZQuery1.ParamByName('categorie').Value := form2.ComboBox3.items.ValueFromIndex[form2.ComboBox3.ItemIndex];
      ZQuery1.ParamByName('description').Value := form2.Memo1.Lines.Text;

      try
        ZQuery1.ExecSQL;
      except
        on E: Exception do
          showmessage('Erreur: ' + E.Message);
      end;
      drawAgendas;
    end;
    ZQuery1.Close;
    clickon := false;
    ismoving := false;
  end
  else
  begin
    // largeur colone
    w := round((PaintBox1.Width - LARGEURCOLHEURES) / 5);
    // hauteur de case
    h := round(PaintBox1.height / 46);
    form2.DateTimePicker1.date := incday(DateTimePicker1.date, floor((xclick - LARGEURCOLHEURES) / w));
    form2.DateTimePicker2.Time := incminute(strToTime('08:00:00'), (floor(yclick / h) * 15) - 15);
    ajouter;
  end;

end;

procedure TForm1.PaintBox1MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; x, y: integer);
begin
  if dblclick then
  begin
    dblclick := false;
    exit;
  end;
  xclick := x;
  yclick := y;
  xmouse := x;
  ymouse := y;
  ctrl := ssCtrl in Shift;
  if FselectedBox <> nil then
    FselectedBox.deselect;
  FselectedBox := Fboxs.intersect(x, y, true);
  if FselectedBox <> nil then
  begin
    FlastSelectedId := FselectedBox.id;
    FselectedBox.select;
    clickon := true;
  end;
  PaintBox1.repaint;
end;

procedure TForm1.PaintBox1MouseMove(Sender: TObject; Shift: TShiftState; x, y: integer);
begin
  xmouse := x;
  ymouse := y;

  if not ismoving then
    ismoving := clickon and ((xmouse > xclick + MOVESTART) or (xmouse < xclick - MOVESTART)) and ((ymouse > yclick + MOVESTART) or (ymouse < yclick - MOVESTART));

  ctrl := ssCtrl in Shift;
  if not ismoving then
  begin
    if FhoveredBox <> nil then
    begin
      if FhoveredBox <> FselectedBox then
        FhoveredBox.deselect;
    end;
    FhoveredBox := Fboxs.intersect(x, y);
    if (FhoveredBox <> nil) and (FhoveredBox <> FselectedBox) then
    begin
      FhoveredBox.hover;
      PaintBox1.Hint := FhoveredBox.description;
      Timer1.Enabled := true;
    end
    else
      Timer1.Enabled := false;
  end
  else
  begin
    if (FselectedBox <> nil) and clickon then
      FselectedBox.move(ctrl);
  end;
  PaintBox1.repaint;
end;

procedure TForm1.PaintBox1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; x, y: integer);
var
  newDate: Tdate;
  newTime: Ttime;
  w, h: integer;
  titre, categorie, description: string;
  duree: int64;
begin
  if clickon and ismoving then
  begin
    // largeur colone
    w := round((PaintBox1.Width - LARGEURCOLHEURES) / 5);
    // hauteur de case
    h := round(PaintBox1.height / 46);
    newDate := incday(DateTimePicker1.date, floor((x - LARGEURCOLHEURES) / w));
    newTime := incminute(strToTime('08:00:00'), (floor(y / h) * 15) - 15);
    if ctrl then
    begin
      // showmessage('Copie vers ' + dateToStr(newDate) + ' ' + timeToStr(newTime));
      ZQuery1.SQL.Text := 'SELECT *,(fin-debut) as duree FROM activites WHERE id=:id';
      ZQuery1.ParamByName('id').Value := FselectedBox.id;
      ZQuery1.Open;
      ZQuery1.First;
      titre := ZQuery1.FieldByName('titre').AsString;
      duree := htexttosec(ZQuery1.FieldByName('duree').AsString);
      categorie := ZQuery1.FieldByName('categorie').AsString;
      description := ZQuery1.FieldByName('description').AsString;
      ZQuery1.Close;

      ZQuery1.SQL.Text := 'INSERT INTO activites(titre,date,debut,fin,categorie,description) VALUES(:titre,:date,:debut,:fin,:categorie,:description)';
      ZQuery1.ParamByName('titre').Value := titre;
      ZQuery1.ParamByName('date').Value := formatDateTime('yyyy-mm-dd', newDate);
      ZQuery1.ParamByName('debut').Value := formatDateTime('HH:nn:00', newTime);
      ZQuery1.ParamByName('fin').Value := formatDateTime('HH:nn:00', incSecond(newTime, duree));
      ZQuery1.ParamByName('categorie').Value := categorie;
      ZQuery1.ParamByName('description').Value := 'Copie - ' + description;
      try
        ZQuery1.ExecSQL;
      except
        on E: Exception do
          showmessage('Erreur: ' + E.Message);
      end;
      drawAgendas;
    end
    else
    begin
      // showmessage('D�place vers ' + dateToStr(newDate) + ' ' + timeToStr(newTime));
      ZQuery1.SQL.Text := 'SELECT (fin-debut) as duree FROM activites WHERE id=:id';
      ZQuery1.ParamByName('id').Value := FselectedBox.id;
      ZQuery1.Open;
      ZQuery1.First;
      duree := htexttosec(ZQuery1.FieldByName('duree').AsString);

      ZQuery1.SQL.Text := 'UPDATE activites set date=:date,debut=:debut,fin=:fin WHERE id=:id';
      ZQuery1.ParamByName('id').Value := FselectedBox.id;
      ZQuery1.ParamByName('date').Value := formatDateTime('yyyy-mm-dd', newDate);
      ZQuery1.ParamByName('debut').Value := formatDateTime('HH:nn:00', newTime);
      ZQuery1.ParamByName('fin').Value := formatDateTime('HH:nn:00', incSecond(newTime, duree));
      try
        ZQuery1.ExecSQL;
      except
        on E: Exception do
          showmessage('Erreur: ' + E.Message);
      end;
      drawAgendas;
    end;
    if FselectedBox <> nil then
      FselectedBox.select;
  end;
  clickon := false;
  ismoving := false;
end;

procedure TForm1.PaintBox1Paint(Sender: TObject);
begin
  PaintBox1.canvas.draw(-1, 0, Fbuffer);
  if ismoving and clickon then
    PaintBox1.canvas.draw(xmouse, ymouse, FbufferMove);
end;

procedure TForm1.PopupMenu1Popup(Sender: TObject);
begin
  // Statistiques1.Enabled := FselectedBox <> nil;
end;

procedure TForm1.statall;
var
  it: Tlistitem;
  totp, p, v: single;
  d, fd, ld, m, y: Word;
  g: string;
begin

  if form5.CheckBox1.Checked then
    g := 'categorie'
  else
    g := 'titre';

  if form5.radiobutton1.Checked then
  begin
    ZQuery1.SQL.Text := 'SELECT sum(fin-debut)as temps,' + g + ' FROM activites GROUP BY ' + g + ' ORDER BY temps DESC';
  end;
  if form5.radiobutton2.Checked then
  begin
    decodeDate(DateTimePicker1.date, y, m, d);
    ld := DaysInMonth(DateTimePicker1.date);
    ZQuery1.SQL.Text := 'SELECT sum(fin-debut)as temps,' + g + ' FROM activites WHERE date BETWEEN :a AND :b GROUP BY ' + g + ' ORDER BY temps DESC';
    ZQuery1.ParamByName('a').Value := encodeDate(y, m, 1);
    ZQuery1.ParamByName('b').Value := encodeDate(y, m, ld);
  end;
  ZQuery1.Open;
  ZQuery1.First;
  totp := 0;
  while not ZQuery1.eof do
  begin
    totp := totp + htexttosec(ZQuery1.FieldByName('temps').AsString);
    ZQuery1.Next;
  end;
  ZQuery1.First;
  form5.listview1.items.beginupdate;
  form5.listview1.items.clear;
  p := 0;
  while not ZQuery1.eof do
  begin
    it := form5.listview1.items.add;
    it.Caption := ZQuery1.FieldByName(g).AsString;
    it.SubItems.add(ZQuery1.FieldByName('temps').AsString);
    v := (htexttosec(ZQuery1.FieldByName('temps').AsString) / totp) * 100;
    p := p + v;
    it.SubItems.add(formatfloat('0.00', v));
    if p > form5.spinedit1.Value then
      break;
    ZQuery1.Next;
  end;
  form5.listview1.items.EndUpdate;
  ZQuery1.Close;
end;

procedure TForm1.Statistiques1Click(Sender: TObject);
var
  f: Tform4;
  d, fd, ld, m, y: Word;
begin
  if FselectedBox = nil then
  begin
    statall;
    form5.Show;
  end
  else
  begin

    decodeDate(DateTimePicker1.date, y, m, d);
    ld := DaysInMonth(DateTimePicker1.date);

    f := Tform4.create(application);
    ZQuery1.SQL.Text := 'SELECT count(a.titre) as n,sum(fin-debut) as tot,a.titre,c.couleur,';
    ZQuery1.SQL.add('(SELECT sum(fin-debut) FROM activites) as totall,');
    ZQuery1.SQL.add('(SELECT sum(fin-debut) FROM activites WHERE date BETWEEN :a AND :b) ');
    ZQuery1.SQL.add('as totmois FROM activites a LEFT JOIN categories c ON c.titre=a.categorie WHERE a.titre=''' + FselectedBox.titre + ''' GROUP BY a.titre,c.couleur');
    ZQuery1.ParamByName('a').Value := encodeDate(y, m, 1);
    ZQuery1.ParamByName('b').Value := encodeDate(y, m, ld);

    ZQuery1.Open;
    ZQuery1.First;

    f.Caption := ZQuery1.FieldByName('titre').AsString;
    f.Edit2.Text := ZQuery1.FieldByName('n').AsString;
    f.Edit1.Text := ZQuery1.FieldByName('tot').AsString;

    f.tot := htexttosec(ZQuery1.FieldByName('tot').AsString);
    f.totmois := htexttosec(ZQuery1.FieldByName('totmois').AsString);
    f.totall := htexttosec(ZQuery1.FieldByName('totall').AsString);
    f.titre := ZQuery1.FieldByName('titre').AsString;
    f.couleur := stringToColor(ZQuery1.FieldByName('couleur').AsString);

    f.Show;
  end;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  application.ActivateHint(mouse.CursorPos);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  DateTimePicker1.date := incday(DateTimePicker1.date, 7);
  drawGrid;
  drawAgendas;
  Label2.Caption := 'Semaine n�' + inttostr(WeekOfTheYear(DateTimePicker1.dateTime));
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  DateTimePicker1.date := incday(DateTimePicker1.date, -7);
  drawGrid;
  drawAgendas;
  Label2.Caption := 'Semaine n�' + inttostr(WeekOfTheYear(DateTimePicker1.dateTime));
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  form3.Show;
end;

procedure TForm1.DateTimePicker1Change(Sender: TObject);
begin
  DateTimePicker1.date := lundi(DateTimePicker1.dateTime);
  drawGrid;
  drawAgendas;
end;

procedure TForm1.drawGrid;
var
  a, b, h, x, y, yt, xt, ha, ya, ht: integer;
  l: Tdate;
  myYear, myMonth, myDay, w: Word;
  duree, debutsec, largeurcoljour: integer;
begin
  largeurcoljour := round((PaintBox1.Width - LARGEURCOLHEURES) / 5);
  FbufferGrid.SetSize(PaintBox1.Width, PaintBox1.height);
  with FbufferGrid.canvas do
  begin
    // init
    font.Color := clgray;
    pen.Style := pssolid;
    brush.Style := bssolid;
    pen.Color := clwhite;
    brush.Color := clwhite;
    // efface
    rectangle(cliprect);
    // hauteur de case
    h := round(PaintBox1.height / 46);
    // hauteur de texte
    ht := textHeight('q�"');
    pen.Color := clgray;
    brush.Style := bsclear;
    // heures
    y := h;
    for b := 8 to 18 do
    begin
      // pile
      y := drawCaseHeures(y, h, inttostr(b) + ':00');
      // 15
      y := drawCaseHeures(y, h, inttostr(b) + ':15');
      // 30
      y := drawCaseHeures(y, h, inttostr(b) + ':30');
      // 45
      y := drawCaseHeures(y, h, inttostr(b) + ':45');
    end;
    // jours
    decodeDate(DateTimePicker1.dateTime, myYear, myMonth, myDay);
    w := WeekOfTheYear(DateTimePicker1.date);
    if (myMonth = 12) and (w = 1) then
      myYear := myYear + 1;
    // lundi de la semaine de la date choisie
    l := StartOfAWeek(myYear, w, 1);
    x := LARGEURCOLHEURES - 1;
    font.Style := [fsbold];
    yt := round((h - ht) / 2);
    for a := 0 to 5 do
    begin
      rectangle(x, 0, x + largeurcoljour, h);
      xt := round((largeurcoljour - textwidth(formatDateTime('dddd d mmm', l))) / 2);
      textrect(rect(x + 1, 1, x + largeurcoljour - 1, h - 1), x + xt, yt, formatDateTime('dddd d mmm', l));
      moveto(x + largeurcoljour - 1, h);
      lineto(x + largeurcoljour - 1, cliprect.Bottom);
      x := x + largeurcoljour - 1;
      l := incday(l, 1);
    end;
  end;
end;

procedure TForm1.drawAgendas;
var
  a, b, h, x, y, yt, xt, ha, ya, ht: integer;
  l: Tdate;
  myYear, myMonth, myDay, w: Word;
  duree, debutsec, largeurcoljour: integer;
begin
  FselectedBox := nil;
  FhoveredBox := nil;
  Fboxs.clear;
  largeurcoljour := round((PaintBox1.Width - LARGEURCOLHEURES) / 5);
  Fbuffer.SetSize(PaintBox1.Width, PaintBox1.height);
  FbufferMove.SetSize(PaintBox1.Width, PaintBox1.height);
  with Fbuffer.canvas do
  begin
    draw(0, 0, FbufferGrid);
    h := round(PaintBox1.height / 46);
    ht := textHeight('q�"');
    pen.Color := clgray;
    brush.Style := bsclear;
    // activit�s
    // jour mois an de la date choisie
    decodeDate(DateTimePicker1.date, myYear, myMonth, myDay);
    w := WeekOfTheYear(DateTimePicker1.date);
    if (myMonth = 12) and (w = 1) then
      myYear := myYear + 1;
    // lundi de la semaine de la date choisie
    l := StartOfAWeek(myYear, w, 1);
    // position x de d�part
    x := LARGEURCOLHEURES - 1;
    // position y 0
    y := h;
    // pas de trait
    pen.Style := psclear;
    for a := 0 to 5 do
    begin
      ZQuery1.SQL.Text := 'SELECT activites.titre,date,debut,fin,description,couleur,id FROM activites LEFT JOIN categories ON activites.categorie=categories.titre WHERE date BETWEEN ''' + dateToStr(l) + ' 00:00:00'' AND ''' + dateToStr(l) + ' 23:59:59''';
      try
        ZQuery1.Open;
        ZQuery1.First;
        while not ZQuery1.eof do
        begin
          // jour mois an de la date � afficher
          decodeDate(ZQuery1.FieldByName('debut').AsDateTime, myYear, myMonth, myDay);
          // calcul la dur�e de l'activit� en secondes
          duree := trunc((ZQuery1.FieldByName('fin').AsDateTime - ZQuery1.FieldByName('debut').AsDateTime) * 86400);
          // taille de l'affichage
          ha := round((duree / 900) * h);
          // debut de la t�che (en secondes)
          debutsec := trunc((ZQuery1.FieldByName('debut').AsDateTime - encodeDateTime(myYear, myMonth, myDay, 8, 0, 0, 0)) * 86400);
          // position de l'affichage
          ya := round((debutsec / 900) * h);
          Fboxs.add(rect(x + 2, y + ya + 1, x + largeurcoljour - 2, y + ya + ha - 2), ZQuery1); // .FieldByName('id').AsInteger);
          // suivant
          ZQuery1.Next;
        end;
      except
        on E: Exception do
          showmessage('Erreur: ' + E.Message);
      end;
      l := incday(l, 1);
      x := x + largeurcoljour - 1;
    end;
  end;
  FselectedBox := Fboxs.select(FlastSelectedId);
  PaintBox1.repaint;
end;

function TForm1.drawCaseHeures(y, h: integer; b: string): integer;
begin
  with FbufferGrid.canvas do
  begin
    brush.Color := clwhite;
    brush.Style := bssolid;
    rectangle(0, y - 1, LARGEURCOLHEURES, y + h);
    pen.Color := $F5F5F5;
    moveto(LARGEURCOLHEURES + 1, y + h - 1);
    lineto(cliprect.Right, y + h - 1);
    pen.Color := clgray;
    textout(10, y - 8, b);
    Result := y + h;
  end;
end;

{ Tbox }

function Tbox.click(x, y: integer): Tbox;
var
  r: Trect;
  buttonSelected: integer;
begin
  Result := self;
  // delete?
  r := rect(Fr.Right - 20, Fr.top, Fr.Right, Fr.top + 20);
  if PtInRect(r, Point(x, y)) then
  begin
    buttonSelected := MessageDlg('Etes vous s�r de vouloir supprimer ' + Ftitre, mtWarning, mbYesNo, 0);
    if buttonSelected = mrYes then
    begin
      Fparent.delete(self);
      Result := nil;
    end;
  end;
end;

constructor Tbox.create(parent: Tboxliste; r: Trect; q: Tzquery);
begin
  Fparent := parent;
  Fr := r;
  Fid := q.FieldByName('id').AsInteger;
  Ftitre := q.FieldByName('titre').AsString;
  Fdescription := q.FieldByName('description').AsString;
  Fcouleur := stringToColor(q.FieldByName('couleur').AsString);
  draw;
end;

procedure Tbox.draw;
begin
  with Fparent.canvas do
  begin
    brush.Style := bssolid;
    brush.Color := Fcouleur;
    pen.Style := psclear;
    font.Style := [];

    if Fstate = 0 then
    begin
      font.Color := clwhite;
      brush.Color := Fcouleur;
    end
    else if Fstate = 3 then
    begin
      brush.Color := clwhite;
      pen.Style := pssolid;
      pen.Color := brush.Color;
      rectangle(Fr);
      pen.Style := psdot;
      pen.Color := clblack;
      font.Style := [fsbold];
      brush.Style := bsclear;
      rectangle(Fr);
    end
    else if (Fstate = 1) or (Fstate = 4) then
    begin
      pen.Style := pssolid;
      pen.Color := clblack;
      font.Style := [fsbold];
      rectangle(Fr);
      brush.Style := bsclear;
      rectangle(Fr);
    end;
    if Fstate = 2 then
    begin
      font.Color := clwhite;
    end;
    // dessine
    if (Fstate <> 3) then
      textrect(Fr, Fr.left + 10, Fr.top + 1, Ftitre);
    if (Fstate = 1) or (Fstate = 2) then
    begin
      textout(Fr.Right - 12, Fr.top + 1, 'x');
    end;
  end;
  // copy pour move
  if Fstate = 1 then
  begin
    with Fparent.FbufferMove.canvas do
    begin
      pen.Style := psclear;
      brush.Color := Fcouleur;
      CopyRect(rect(0, 0, Fr.Right - Fr.left, Fr.Bottom - Fr.top), Fparent.canvas, Fr);
      rectangle(Fr.Right - 12 - Fr.left, 1, Fr.Right - Fr.left - 1, 21);
    end;
  end;
end;

procedure Tbox.hover;
begin
  Fstate := 2;
  draw;
end;

procedure Tbox.move(ctrl: boolean);
begin
  if ctrl then
    Fstate := 4
  else
    Fstate := 3;
  draw;
end;

procedure Tbox.select;
begin
  Fstate := 1;
  Fparent.FbufferMove.SetSize(Fr.Right - Fr.left, Fr.Bottom - Fr.top);
  draw;
end;

procedure Tbox.delete;
begin
  if assigned(Fparent.FonDelete) then
    Fparent.FonDelete(self);
end;

procedure Tbox.deselect;
begin
  Fstate := 0;
  draw;
end;

{ TboxListe }

procedure Tboxliste.add(r: Trect; q: Tzquery);
var
  b: Tbox;
begin
  b := Tbox.create(self, r, q);
  Fitems.add(b);
end;

procedure Tboxliste.clear;
var
  I: integer;
begin
  for I := 0 to Fitems.Count - 1 do
  begin
    Tbox(Fitems[I]).free;
    Fitems[I] := nil;
  end;
  Fitems.clear;
end;

constructor Tboxliste.create(canvas: Tcanvas; bufferMove: Tbitmap);
begin
  Fcanvas := canvas;
  FbufferMove := bufferMove;
  Fitems := Tlist.create;
end;

procedure Tboxliste.delete(box: Tbox);
var
  I: integer;
begin
  for I := 0 to Fitems.Count - 1 do
    if Tbox(Fitems[I]).id = box.id then
    begin
      Fitems.delete(I);
      break;
    end;
  box.delete;
end;

destructor Tboxliste.free;
begin
  clear;
  Fitems.free;
end;

function Tboxliste.getItem(index: integer): Tbox;
begin
  Result := Fitems[index];
end;

function Tboxliste.intersect(x, y: integer; click: boolean = false): Tbox;
var
  I: integer;
begin
  Result := nil;
  for I := Fitems.Count - 1 downto 0 do
  begin
    if PtInRect(Tbox(Fitems[I]).r, Point(x, y)) then
    begin
      Result := Tbox(Fitems[I]);
      if click then
      begin
        Result := Result.click(x, y);
      end;
      exit;
    end;
  end;
end;

function Tboxliste.select(id: integer): Tbox;
var
  I: integer;
begin
  Result := nil;
  for I := 0 to Fitems.Count - 1 do
    if Tbox(Fitems[I]).id = id then
    begin
      Result := Tbox(Fitems[I]);
      Tbox(Fitems[I]).select;
      exit;
    end;
end;

procedure Tboxliste.setItem(index: integer; val: Tbox);
begin
  Fitems[index] := val;
end;

end.
