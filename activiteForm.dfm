object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Ajouter'
  ClientHeight = 338
  ClientWidth = 545
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clGray
  Font.Height = -16
  Font.Name = 'Segoe UI Light'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  DesignSize = (
    545
    338)
  PixelsPerInch = 96
  TextHeight = 21
  object Label1: TLabel
    Left = 4
    Top = 36
    Width = 29
    Height = 21
    Caption = 'Titre'
  end
  object Label2: TLabel
    Left = 4
    Top = 88
    Width = 75
    Height = 21
    Caption = 'Description'
  end
  object Label3: TLabel
    Left = 438
    Top = 36
    Width = 32
    Height = 21
    Anchors = [akTop, akRight]
    Caption = 'Date'
  end
  object Label4: TLabel
    Left = 8
    Top = 216
    Width = 42
    Height = 21
    Caption = 'D'#233'but'
  end
  object Label5: TLabel
    Left = 128
    Top = 216
    Width = 19
    Height = 21
    Caption = 'Fin'
  end
  object Label6: TLabel
    Left = 8
    Top = 289
    Width = 65
    Height = 21
    Anchors = [akLeft, akBottom]
    Caption = 'Cat'#233'gorie'
  end
  object Label7: TLabel
    Left = 260
    Top = 221
    Width = 41
    Height = 21
    Anchors = [akLeft, akBottom]
    Caption = 'Dur'#233'e'
  end
  object Edit1: TEdit
    Left = 4
    Top = 60
    Width = 423
    Height = 29
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Text = 'Edit1'
    OnChange = Edit1Change
    OnDblClick = Edit1DblClick
    OnExit = Edit1Exit
    OnKeyDown = Edit1KeyDown
  end
  object Memo1: TMemo
    Left = 4
    Top = 112
    Width = 535
    Height = 101
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object DateTimePicker1: TDateTimePicker
    Left = 438
    Top = 60
    Width = 101
    Height = 29
    Anchors = [akTop, akRight]
    Date = 41645.401377997690000000
    Time = 41645.401377997690000000
    TabOrder = 2
  end
  object ComboBox3: TComboBox
    Left = 84
    Top = 289
    Width = 145
    Height = 22
    Style = csOwnerDrawFixed
    Anchors = [akLeft, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -13
    Font.Name = 'Segoe UI Light'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnChange = ComboBox3Change
    OnDrawItem = ComboBox3DrawItem
  end
  object Button1: TButton
    Left = 466
    Top = 309
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Ok'
    Enabled = False
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 382
    Top = 309
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Annuler'
    TabOrder = 5
    OnClick = Button2Click
  end
  object DateTimePicker2: TDateTimePicker
    Left = 4
    Top = 241
    Width = 81
    Height = 29
    Anchors = [akLeft, akBottom]
    Date = 41645.401377997690000000
    Format = 'HH:mm'
    Time = 41645.401377997690000000
    Kind = dtkTime
    TabOrder = 6
    OnChange = DateTimePicker2Change
  end
  object DateTimePicker3: TDateTimePicker
    Left = 124
    Top = 241
    Width = 81
    Height = 29
    Anchors = [akLeft, akBottom]
    Date = 41645.401377997690000000
    Format = 'HH:mm'
    Time = 41645.401377997690000000
    Kind = dtkTime
    TabOrder = 7
    OnChange = DateTimePicker3Change
  end
  object ListBox1: TListBox
    Left = 4
    Top = 28
    Width = 73
    Height = 214
    Anchors = [akLeft, akBottom]
    ItemHeight = 21
    TabOrder = 8
    Visible = False
    OnClick = ListBox1Click
  end
  object Edit2: TEdit
    Left = 256
    Top = 241
    Width = 81
    Height = 29
    Anchors = [akLeft, akBottom]
    TabOrder = 9
    Text = '01:00'
  end
  object Button3: TButton
    Left = 88
    Top = 241
    Width = 21
    Height = 21
    Anchors = [akLeft, akBottom]
    Caption = '/\'
    TabOrder = 10
    OnClick = Button3Click
  end
  object ListBox2: TListBox
    Left = 124
    Top = 28
    Width = 77
    Height = 214
    Anchors = [akLeft, akBottom]
    ItemHeight = 21
    TabOrder = 11
    Visible = False
    OnClick = ListBox2Click
  end
  object Button4: TButton
    Left = 208
    Top = 241
    Width = 21
    Height = 21
    Anchors = [akLeft, akBottom]
    Caption = '/\'
    TabOrder = 12
    OnClick = Button4Click
  end
  object ListBox3: TListBox
    Left = 4
    Top = 88
    Width = 421
    Height = 145
    ItemHeight = 21
    TabOrder = 13
    Visible = False
    OnClick = ListBox3Click
  end
end
