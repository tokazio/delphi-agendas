object Form5: TForm5
  Left = 0
  Top = 0
  Caption = 'Statistiques'
  ClientHeight = 433
  ClientWidth = 539
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Segoe UI Light'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 21
  object Label1: TLabel
    Left = 56
    Top = 32
    Width = 133
    Height = 21
    Caption = '% du temps pass'#233' '#224
  end
  object ListView1: TListView
    AlignWithMargins = True
    Left = 1
    Top = 67
    Width = 537
    Height = 335
    Margins.Left = 1
    Margins.Top = 0
    Margins.Right = 1
    Margins.Bottom = 1
    Align = alBottom
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsNone
    Columns = <
      item
        AutoSize = True
        Caption = 'Nom'
      end
      item
        Caption = 'Temps'
        Width = 100
      end
      item
        Caption = '%'
      end>
    GridLines = True
    ReadOnly = True
    RowSelect = True
    TabOrder = 0
    ViewStyle = vsReport
    ExplicitTop = 68
  end
  object SpinEdit1: TSpinEdit
    Left = 4
    Top = 28
    Width = 49
    Height = 31
    MaxValue = 0
    MinValue = 0
    TabOrder = 1
    Value = 80
    OnChange = SpinEdit1Change
  end
  object CheckBox1: TCheckBox
    Left = 378
    Top = 36
    Width = 153
    Height = 17
    Caption = 'Grouper par cat'#233'gories'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI Light'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = CheckBox1Click
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 1
    Top = 403
    Width = 537
    Height = 29
    Margins.Left = 1
    Margins.Top = 0
    Margins.Right = 1
    Margins.Bottom = 1
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 3
    ExplicitLeft = 0
    ExplicitTop = 404
    ExplicitWidth = 539
    object metroSeparator1: TmetroSeparator
      Left = 0
      Top = 0
      Width = 537
      Height = 12
      Margins.Left = 1
      Margins.Top = 0
      Margins.Right = 1
      Margins.Bottom = 1
      align = alTop
      drawHeight = 1
      drawSide = msTop
      ExplicitTop = 28
      ExplicitWidth = 539
    end
    object RadioButton1: TRadioButton
      Left = 6
      Top = 8
      Width = 75
      Height = 17
      Caption = 'Au mois'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI Light'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TabStop = True
      OnClick = RadioButton1Click
    end
    object RadioButton2: TRadioButton
      Left = 86
      Top = 8
      Width = 113
      Height = 17
      Caption = 'Au total'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI Light'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = RadioButton1Click
    end
  end
end
