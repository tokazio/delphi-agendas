unit camembertU;

interface

uses graphics, windows, types, Math, sysutils, classes;

type

  Ttexte = class
  Private
    Fpt: Tpoint;
    Ftexte: string;
    function getX: integer;
    function getY: integer;
  Published
    property pt: Tpoint read Fpt;
    property texte: string read Ftexte;
    property x: integer read getX;
    property y: integer read getY;
  Public
    constructor create(pt: Tpoint; texte: string);
  end;

function GetPositionForAngle(const Center: Tpoint; const Radius: integer; const Angle: Single): Tpoint;
procedure PlotArc(const Canvas: TCanvas; const Center: Tpoint; const Radius: integer; const StartAngle: Single; const StopAngle: Single);
procedure drawCamembert(c: TCanvas; list: TStringList; couleurs: TStringList);
function HSLtoRGB(H, S, L: double): TColor; overload;
function HSLtoRGB(H: integer; S, L: double): TColor; overload;
function HSLtoRGB(H, S, L: integer): TColor; overload;

implementation

function HSLtoRGB(H, S, L: double): TColor; overload;
var
  M1, M2: double;

  function HueToColourValue(Hue: double): byte;
  var
    V: double;
  begin
    if Hue < 0 then
      Hue := Hue + 1
    else if Hue > 1 then
      Hue := Hue - 1;

    if 6 * Hue < 1 then
      V := M1 + (M2 - M1) * Hue * 6
    else if 2 * Hue < 1 then
      V := M2
    else if 3 * Hue < 2 then
      V := M1 + (M2 - M1) * (2 / 3 - Hue) * 6
    else
      V := M1;
    Result := round(255 * V)
  end;

var
  R, G, B: byte;
begin
  if S = 0 then
  begin
    R := round(255 * L);
    G := R;
    B := R
  end
  else
  begin
    if L <= 0.5 then
      M2 := L * (1 + S)
    else
      M2 := L + S - L * S;
    M1 := 2 * L - M2;
    R := HueToColourValue(H + 1 / 3);
    G := HueToColourValue(H);
    B := HueToColourValue(H - 1 / 3)
  end;

  Result := RGB(R, G, B)
end;

function HSLtoRGB(H: integer; S, L: double): TColor; overload;
begin
  Result := HSLtoRGB(H / 360, S, L);
end;

function HSLtoRGB(H, S, L: integer): TColor; overload;
begin
  Result := HSLtoRGB(H, S / 100, L / 100);
end;

(*
  * This function splits the red-green-blue colour wheel into n equally spaced divisions
  * and returns the colour from a particular division.
  *
  * @param index The index of the colour to return, the range is 0 - (count-1)
  * @param count The number of divisions to split the HSV colour wheel into
  * @return A java.awt.Color object containing the colour.
  * @author HughesR
*)
function getSpacedOutColour(index, count: integer): TColor;
var
  Hue, saturation, brightness: Single;
  R, G, B: byte;
begin
  saturation := 0.9; // Saturation
  brightness := 0.6; // Brightness
  Hue := index / count;
  Result := HSLtoRGB(Hue, saturation, brightness);
end;

function GetPositionForAngle(const Center: Tpoint; const Radius: integer; const Angle: Single): Tpoint;
var
  CosAngle: Extended;
  SinAngle: Extended;
begin
  SinCos(DegToRad(Angle), SinAngle, CosAngle);
  Result.x := round(Center.x + Radius * SinAngle);
  Result.y := round(Center.y - Radius * CosAngle);
end;

procedure PlotArc(const Canvas: TCanvas; const Center: Tpoint; const Radius: integer; const StartAngle: Single; const StopAngle: Single);
  function GetPositionForAngle(const Angle: Single): Tpoint;
  var
    CosAngle: Extended;
    SinAngle: Extended;
  begin
    SinCos(DegToRad(Angle), SinAngle, CosAngle);
    Result.x := round(Center.x + Radius * SinAngle);
    Result.y := round(Center.y - Radius * CosAngle);
  end;

var
  Index: integer;
begin
  with GetPositionForAngle(StartAngle) do
    Canvas.MoveTo(x, y);

  for Index := Ceil(StartAngle) to Floor(StopAngle) do
    with GetPositionForAngle(Index) do
      Canvas.LineTo(x, y);

  with GetPositionForAngle(StopAngle) do
    Canvas.LineTo(x, y);
end;

// Liste avec categorie/heures
// dernier de la liste est le total
//

procedure drawCamembert(c: TCanvas; list: TStringList; couleurs: TStringList);
var
  I, R, r2, w: integer;
  p, t, a, B: Single;
  pt, pt1, pt2, cpt: Tpoint;
  textes: Tlist;
begin
  textes := Tlist.create;
  // c.SetSize(PaintBox1.Width, PaintBox1.height);
  if c.ClipRect.Right < c.ClipRect.Bottom then
    w := c.ClipRect.Right
  else
    w := c.ClipRect.Bottom;
  with c do
  begin
    brush.Color := clwhite;
    pen.Color := clwhite;
    rectangle(ClipRect);
    pen.Color := clblack;
    // total en sec
    trystrToFloat(list.valuefromindex[list.count - 1], t);
    B := 0;
    for I := 0 to list.count - 2 do
    begin
      // couleur
      pen.Color := clblack;
      if couleurs.Values[list.Names[I]] <> '' then
        brush.Color := stringToColor(couleurs.Values[list.Names[I]])
      else
        brush.Color := getSpacedOutColour(I, list.count);
      // pourcentage
      trystrToFloat(list.valuefromindex[I], p);
      p := p / t;
      // angle en deg
      a := p * 360;
      // point centre
      cpt := point(round(w / 2) + 50, round(w / 2));
      // rayon fromage
      R := round(w / 3);
      // rayon texte
      r2 := round(w / 3);
      // dessine l'arc de la portion de fromage
      PlotArc(c, cpt, R, B, B + a);
      LineTo(cpt.x, cpt.y);
      // dessine le cot� a de la portion de fromage
      MoveTo(cpt.x, cpt.y);
      pt := GetPositionForAngle(cpt, R, B);
      LineTo(pt.x, pt.y);
      // dessine le cot� b de la portion de fromage
      // MoveTo(cpt.x,cpt.y);
      // pt := GetPositionForAngle(cpt, r, b + a);
      // LineTo(pt.X,pt.y);
      // point au milieu de la portion de fromage
      pt1 := GetPositionForAngle(cpt, round(R * 0.8), B + (((B + a) - B) / 2));
      // peint   TFillStyle = (fsSurface, fsBorder);
      floodFill(pt1.x, pt1.y, pen.Color, fsBorder);

      // contour portion----
      pen.Color := clwhite;
      // dessine l'arc de la portion de fromage
      PlotArc(c, cpt, R, B, B + a);
      LineTo(cpt.x, cpt.y);
      // dessine le cot� a de la portion de fromage
      MoveTo(cpt.x, cpt.y);
      pt := GetPositionForAngle(cpt, R, B);
      LineTo(pt.x, pt.y);
      // dessine le cot� b de la portion de fromage
      MoveTo(cpt.x, cpt.y);
      pt := GetPositionForAngle(cpt, R, B + a);
      LineTo(pt.x, pt.y);
      // ---

      // ---
      // fleche vers texte
      pen.Color := clblack;
      MoveTo(pt1.x, pt1.y);
      // ecrit le titre de cat�gorie
      pt2 := GetPositionForAngle(cpt, round(R * 1.4), B + (((B + a) - B) / 2));
      LineTo(pt2.x, pt2.y);
      // ajoute l'�tiquette de valeur � la liste pour �crire � la fin par dessus les portions de fromage
      textes.add(Ttexte.create(pt1, inttostr(round(p * 100)) + '%'));
      font.Color := clblack;
      brush.Style := bsclear;
      pt2.x := pt2.x - round(textWidth(list.Names[I]) / 2);
      brush.Color := clwhite;
      brush.Style := bssolid;
      textout(pt2.x, pt2.y + 2, list.Names[I]);
      // fin de l'arc devient d�but de l'autre
      B := B + a;
      // �tiquettes
      brush.Style := bsclear;
      font.Color := clwhite;
    end;
    for I := 0 to textes.count - 1 do
      textout(Ttexte(textes[I]).x, Ttexte(textes[I]).y, Ttexte(textes[I]).texte);

    brush.Color := clwhite;
    brush.Style := bssolid;
    pen.color:=clwhite;
    Ellipse(cpt.x - round(w / 8), cpt.y - round(w / 8), cpt.x + round(w / 8), cpt.y + round(w / 8));

  end;
  for I := 0 to textes.count - 1 do
  begin
    Ttexte(textes[I]).Free;
    textes[I] := nil;
  end;
  textes.Free;
end;

{ Ttexte }

constructor Ttexte.create(pt: Tpoint; texte: string);
begin
  Fpt := pt;
  Ftexte := texte;
end;

function Ttexte.getX: integer;
begin
  Result := Fpt.x;
end;

function Ttexte.getY: integer;
begin
  Result := Fpt.y;
end;

end.
